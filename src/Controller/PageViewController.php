<?php

namespace Drupal\views_infinite_scroll\Controller;

use Drupal\Core\Controller\ControllerBase;

class PageViewController extends ControllerBase {
	
	
	public function storePageViewData() {
		$ip = \Drupal::request ()->getClientIp ();
		
		// this doens't get used.
		$pagename = \Drupal::request ()->get ( 'pagename' );
		
		$language = \Drupal::request ()->get ( 'language' );
		
		$browser_name = $this->getBrowser ();
		
		db_merge ( 'page_info' )->key ( array (
				'user_ip' => $ip 
		) )->fields ( array (
				'browser_name' => $browser_name,
				'browser_lang' => $language 
		) )->expression ( 'pageview_count', 'pageview_count + 1' )->execute ();
		
		return [ ];
	}
	
	// function for detecting the browser name.
	function getBrowser() {
		if (strpos ( $_SERVER ['HTTP_USER_AGENT'], 'Gecko' )) {
			if (strpos ( $_SERVER ['HTTP_USER_AGENT'], 'Netscape' )) {
				$browser = 'Netscape (Gecko/Netscape)';
			} else if (strpos ( $_SERVER ['HTTP_USER_AGENT'], 'Firefox' )) {
				$browser = 'Mozilla Firefox (Gecko/Firefox)';
			} else {
				$browser = 'Mozilla (Gecko/Mozilla)';
			}
		} else if (strpos ( $_SERVER ['HTTP_USER_AGENT'], 'MSIE' )) {
			if (strpos ( $_SERVER ['HTTP_USER_AGENT'], 'Opera' )) {
				$browser = 'Opera (MSIE/Opera/Compatible)';
			} else {
				$browser = 'Internet Explorer (MSIE/Compatible)';
			}
		} else {
			$browser = 'Others browsers';
		}
		
		return $browser;
	}
}
